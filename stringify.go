package main

func stringify(dict map[Token]string, tokens []Token) []string {
	output := []string{}
	for _, token := range tokens {
		stringified := dict[token]
		if stringified != "" {
			output = append(output, stringified)
		}
	}
	return output
}
