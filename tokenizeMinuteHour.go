package main

type TokenizeMinuteHour struct {
	Hour   int
	Tens   int
	Ones   int
	output []Token
}

func (t TokenizeMinuteHour) tokenize(hour int, tens int, ones int) []Token {
	t.Hour = hour
	t.Tens = tens
	t.Ones = ones

	t.output = []Token{SPREFIX}

	switch 10*t.Tens + t.Ones {
	case 5, 55:
		t.append(M5)
		t.forwardPastOrTo()
	case 25, 35:
		t.append(M5)
		switch t.isPast() {
		case true:
			t.append(STO)
		case false:
			t.append(SPAST)
		}
		t.transHalf()
	case 10, 50:
		t.append(M10)
		t.forwardPastOrTo()
	case 15, 45:
		t.append(M15)
		t.forwardPastOrTo()
	case 20, 40:
		t.append(M20)
		t.forwardPastOrTo()
	case 30:
		t.transHalf()
	default:
		t.transHour()
	}

	t.append(SHOUR)
	t.append(SPOSTFIX)
	return t.output
}

func (t *TokenizeMinuteHour) forwardPastOrTo() {
	if t.isPast() {
		t.append(SPAST)
		t.transHour()
	} else {
		t.append(STO)
		t.transHourIncr()
	}
}

func (t *TokenizeMinuteHour) isPast() bool {
	return t.Tens < 3
}

func (t *TokenizeMinuteHour) transHalf() {
	t.append(M30)
	t.transHourIncr()
}

func (t *TokenizeMinuteHour) transHour() {
	t.append(tokenizeHour(t.Hour))
}

func (t *TokenizeMinuteHour) transHourIncr() {
	t.append(tokenizeHour(t.Hour + 1))
}

func (t *TokenizeMinuteHour) append(token Token) {
	t.output = append(t.output, token)
}
