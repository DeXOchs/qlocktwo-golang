package main

func DictDeDe() map[Token]string {
	return map[Token]string{
		H1:       "eins",
		H2:       "zwei",
		H3:       "drei",
		H4:       "vier",
		H5:       "fünf",
		H6:       "sechs",
		H7:       "sieben",
		H8:       "acht",
		H9:       "neune",
		H10:      "zehn",
		H11:      "elf",
		H12:      "zwölf",
		M5:       "fünf",
		M10:      "zehn",
		M15:      "viertel",
		M20:      "zwanzig",
		M30:      "halb",
		SPAST:    "nach",
		STO:      "vor",
		SPREFIX:  "es ist",
		SPOSTFIX: "",
		SHOUR:    "uhr",
		ERR:      "ERROR"}
}
