package main

type ITokenize interface {
	tokenize(hour int, tens int, ones int) []Token
}
