package main

func DictDeBa() map[Token]string {
	return map[Token]string{
		H1:       "einse",
		H2:       "zweie",
		H3:       "dreie",
		H4:       "viere",
		H5:       "fünfe",
		H6:       "sechse",
		H7:       "siebene",
		H8:       "achte",
		H9:       "neune",
		H10:      "zehne",
		H11:      "elfe",
		H12:      "zwölfe",
		M5:       "fünf",
		M10:      "zehn",
		M15:      "viertel",
		M20:      "zwanzig",
		M30:      "halber",
		SPAST:    "nach",
		STO:      "vor",
		SPREFIX:  "es isch",
		SPOSTFIX: "",
		SHOUR:    "",
		ERR:      "ERROR"}
}
