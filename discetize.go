package main

import (
	"math"
	"time"
)

func discretizeTime(time time.Time) (int, int, int) {
	hour := func(m int, h int) int {
		if m >= 58 {
			return (h + 1) % 12
		}
		return h % 12
	}
	tens := func(m int) int {
		if (m % 10) > 7 {
			return (m / 10) + 1
		} else {
			return m / 10
		}
	}
	ones := func(m int) int {
		o := float64(m % 10)
		o = math.Mod(o, 8)
		o /= 5
		o = math.Round(o)
		return int(o * 5)
	}

	return hour(time.Minute(), time.Hour()), tens(time.Minute()), ones(time.Minute())
}
