package main

func DictEnGb() map[Token]string {
	return map[Token]string{
		H1:       "one",
		H2:       "two",
		H3:       "three",
		H4:       "four",
		H5:       "five",
		H6:       "six",
		H7:       "seven",
		H8:       "eight",
		H9:       "nine",
		H10:      "ten",
		H11:      "eleven",
		H12:      "twelfe",
		M5:       "five",
		M10:      "ten",
		M15:      "quarter",
		M20:      "twenty",
		M30:      "half",
		SPAST:    "past",
		STO:      "to",
		SPREFIX:  "it is",
		SPOSTFIX: "",
		SHOUR:    "o'clock",
		ERR:      "ERROR"}
}
