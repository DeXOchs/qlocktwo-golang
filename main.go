package main

import (
	"time"
)

func main() {
	qlocktwo := QlockTwo{tokenizer: TokenizeMinuteHour{}, dict: DictDeBa()}
	for {
		go qlocktwo.parseTime(time.Now())
		time.Sleep(time.Minute)
	}
}
