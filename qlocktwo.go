package main

import (
	"strings"
	"time"
)

type QlockTwo struct {
	tokenizer ITokenize
	dict      map[Token]string
}

func (q QlockTwo) parseTime(time time.Time) {
	hour, tens, ones := discretizeTime(time)
	tokens := q.tokenizer.tokenize(hour, tens, ones)
	stringified := stringify(q.dict, tokens)
	text := strings.Join(stringified[:], " ")
	println(text)
}
