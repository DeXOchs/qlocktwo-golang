package main

func tokenizeHour(hour int) Token {
	switch hour {
	case 1:
		return H1
	case 2:
		return H2
	case 3:
		return H3
	case 4:
		return H4
	case 5:
		return H5
	case 6:
		return H6
	case 7:
		return H7
	case 8:
		return H8
	case 9:
		return H9
	case 10:
		return H10
	case 11:
		return H11
	case 0, 12:
		return H12
	default:
		return ERR
	}
}
